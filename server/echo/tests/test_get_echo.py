import hashlib 
from datetime import datetime
from echo.controllers import get_echo


def test_get_echo():
    action_name = 'echo'
    data = 'Some data'

    hash_obj = hashlib.sha1()
    hash_obj.update(
        str(datetime.now().timestamp()).encode()
    )

    request = {
        'action': action_name,
        'time': datetime.now().timestamp(),
        'data': data,
        'user': hash_obj.hexdigest()
    }

    expected = {
        'action': action_name,
        'user': None,
        'time': None,
        'data': data,
        'code': 200
    }

    response = get_echo(request)

    assert expected.get('data') == response.get('data')
